module.exports = {
  LOGIN: 'https://us-central1-picam-255511.cloudfunctions.net/api/authentication/login',
  REGISTER: 'https://us-central1-picam-255511.cloudfunctions.net/api/authentication/register',
  GET_TOKEN: 'https://us-central1-picam-255511.cloudfunctions.net/api/authentication/get_token',
  UPLOAD: 'https://us-central1-picam-255511.cloudfunctions.net/api/image/upload',
  DELETE_PHOTOS: 'https://us-central1-picam-255511.cloudfunctions.net/api/image/delete',
  GET_LATEST_IMAGE_INFO: 'https://us-central1-picam-255511.cloudfunctions.net/api/image/get_info',
  GET_IMAGES_TOTAL_COUNT: 'https://us-central1-picam-255511.cloudfunctions.net/api/image/get_total_count',
  GET_RECORD: 'https://us-central1-picam-255511.cloudfunctions.net/api/record/get_record',
  GET_RECORDS_FROM_PAST_WEEK: 'https://us-central1-picam-255511.cloudfunctions.net/api/record/get_records_past_week',
  GET_LATEST_RECORD: 'https://us-central1-picam-255511.cloudfunctions.net/api/record/get_latest_record',
  ADD_MEMBER: 'https://us-central1-picam-255511.cloudfunctions.net/api/member/add_member',
  DELETE_MEMBER: 'https://us-central1-picam-255511.cloudfunctions.net/api/member/delete',
  GET_MEMBER_INFO: 'https://us-central1-picam-255511.cloudfunctions.net/api/member/get_info',
  EXPORT_DATA: 'http://localhost:5000/picam-255511/us-central1/export_google_sheet',
  GET_RECORDS_BY_DATE: 'https://us-central1-picam-255511.cloudfunctions.net/api/record/get_record_by_date'
}
