const https = require('https')

const options = {
  hostname: 'us-central1-picam-255511.cloudfunctions.net',
  port: 443,
  path: '/getToken',
  method: 'GET'
}

exports.getToken = function () {
  const req = https.request(options, res => {
    res.on('data', function (data) {
      var authWeb = JSON.parse(data)
      window.location.href = authWeb
    })
  })

  req.end()
}
