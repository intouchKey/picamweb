
const routes = [
  {
    path: '/',
    component: () => import('layouts/Login.vue')
  },
  {
    path: '/google_auth',
    component: () => import('layouts/GoogleAuth.vue')
  },
  {
    path: '/temporary',
    component: () => import('layouts/Temporary.vue')
  },
  {
    path: '/mainfeed',
    component: () => import('layouts/Mainfeed.vue')
  },
  {
    path: '/upload_image',
    component: () => import('layouts/UploadImage.vue')
  },
  {
    path: '/export_records',
    component: () => import('layouts/ExportRecords.vue')
  },
  {
    path: '/gallery',
    component: () => import('layouts/Gallery.vue')
  },
  {
    path: '/Members',
    component: () => import('layouts/Members.vue')
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
